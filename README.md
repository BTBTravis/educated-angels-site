# Educated Angels Site

A website for the nonprofit organization Educated Angels

## Architecture

At the core, the site is a Wordpress site built with
[Bedrock](https://roots.io/bedrock/) and a custom theme started with
[Sage](https://roots.io/sage/). So this repository includes the site, the theme,
as well as some custom plugins. The site is also a proof of concept in running
WordPress in a docker swarm accompanied by helper microservices. Currently, the
entire site consists of this repository which builds the main Wordpress
container, an [ea-helper node.js microservice container](https://gitlab.com/BTBTravis/educated-angels-helper)
responsibility for contacting 3rd party services like facebook, and a MySQL container. 

## Plugins

> **Note:** this site's plugins are managed with composer and can't be installed within the GUI.
> For details on how to install plugins please see [bedrock docs](https://roots.io/bedrock/docs/composer)

There is one exception to composer managed plugins and that is custom plugins.
These plugins are part of this repository as they are quite small and not
reused, yet. 

### Custom Plugin 1: Educated Angels Facebook Events Page Block

This plugin is a custom block for rendering facebook events. It simply fetches
events via http from the helper,
[ea-helper](https://gitlab.com/BTBTravis/educated-angels-helper), templates and
renders them. The HTML is meant to be generic event blocks so the styles live in
the ea-theme itself. 

### Custom Plugin 2: Educated Angels Page Blocks

This plugin provides a simple block to highlight posts. 

## CI/CD

Currently, this site has a two-step continuous integration of which the details
can be viewed in `/.gitlab-ci.yml`. When master is merged into production either
manually or through a pull ruquest, a build starts. First, a node based container
is brought up and builds the Javascript and CSS assets. After that extra files
are deleted and everything is packaged into an artifact which is passed to the
second container. This container is a special docker in docker container which
takes the artifact and builds a new production container of the site. The
product container is then pushed to the Gitlab repository to await
deployment. 

Deployment itself is handled via an Ansible playbook `deploy.yml`. This
playbook checks if the docker swarm is healthy then tells the stack to redeploy
causing the current image in the swarm to check against the one in the Gitlab registry and update.

## Built With

* [Bedrock](https://roots.io/bedrock/) and a custome theme started with
* [Sage](https://roots.io/sage/). So this repository includes the site, the theme,


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/BTBTravis/educated-angels-site/tags). 

## Authors

* **Travis Shears** - *Initial work* - https://travisshears.com

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
