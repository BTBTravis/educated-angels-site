.PHONY: help
# vim spaces --> tabs:	https://stackoverflow.com/questions/9104706/how-can-i-convert-spaces-to-tabs-in-vim-or-linux
BUILD ?= `git rev-parse --short HEAD`

help:
		@echo "$(BUILD)"
		@perl -nle'print $& if m{^[a-zA-Z_-]+:.*?## .*$$}' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

build: ## Build the Docker image
				(cd ./bedrock/web/app/themes/ea-theme && yarn build:production)
				docker build -f Dockerfile-prod -t ea-wordpress . -t 853019563312.dkr.ecr.eu-central-1.amazonaws.com/ea-wordpress:latest -t ea-wordpress-$(BUILD)

build-dev: ## Build the Docker image
				docker build -t ea-wordpress-dev . -t ea-wordpress-dev-$(BUILD)

run: ## Run the app in Docker
				docker-compose up -d
				cd ./bedrock/web/app/themes/ea-theme && yarn start
