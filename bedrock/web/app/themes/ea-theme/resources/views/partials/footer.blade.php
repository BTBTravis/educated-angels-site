<footer class="content-info">
  <div class="container">
    @php dynamic_sidebar('sidebar-footer') @endphp
  </div>
  <div class="footer-bar">
    <p>© Educated Angels</p>
    <p class="footer-bar__left">Phone: 254-462-7712</p>
    <p><a class="footer-bar__link footer-bar__right" href="/privacy-policy">Privacy Policy</a></p>
    <p class="footer-bar__left"><a class="footer-bar__link" href="mailto:info@educatedangels.org">Email: info@educatedangels.org</a></p>
  </div>
</footer>
<div class="main-menu__wrapper">
  <nav class="main-menu">
    <i class="main-menu__close-icon fa fa-times"></i>
    @if (has_nav_menu('primary_navigation'))
      {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
    @endif
  </nav>
</div>
