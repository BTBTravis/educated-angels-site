<header class="banner">
  <div class="container header-bar">
    <a href="{{ home_url('/') }}">
      <img alt="logo" class="header-bar__logo" src="@asset('images/ea-logo-full-sm-clear.png')" />
    </a>
    <i class="header-bar__menu-icon fa fa-bars"></i>
    <div class="full-menu__wrapper">
      <nav class="full-menu">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
      </nav>
    </div>
  </div>
</header>
