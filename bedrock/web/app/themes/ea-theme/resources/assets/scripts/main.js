// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
  common,
  // Home page
  home,
  // About Us page, note the change from about-us to aboutUs.
  aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

// Main Menu
(() => {
    let body = document.querySelector('body');
    let menuOpenIconEl = document.querySelector('.header-bar__menu-icon');
    let menuCloseIconEl = document.querySelector('.main-menu__close-icon');
    let menuLinkEls = Array.from(document.querySelectorAll('.main-menu a'));

    function closeMenu() {
        body.classList.remove('hasOpenMenu');
        setTimeout(() => body.classList.remove('hasOpenMenuWrapper'), 100);
    }

    function openMenu() {
        body.classList.add('hasOpenMenuWrapper');
        setTimeout(() => body.classList.add('hasOpenMenu'), 100);
    }
    menuCloseIconEl.addEventListener('click', () => {
        closeMenu();
    });

    menuOpenIconEl.addEventListener('click', () => {
        openMenu();
    });

    menuLinkEls.forEach(link => {
        link.addEventListener('click', () => closeMenu());
    });
})();
