<?php
/**
 * Plugin Name: Educated Angels Page Blocks
 * Plugin URI: https://www.travisshears.com
 * Description: A custom block for rendering a post previews
 * Version: 0.0.1
 * Author: Travis Shears
 * Author URI: https://www.travisshears.com
 **/

function ea_page_enqueue_blocks()
{
    wp_enqueue_script(
        'ea-page-block',
        plugins_url('block.js', __FILE__),
        array('wp-blocks', 'wp-element')
    );
}

add_action('enqueue_block_editor_assets', 'ea_page_enqueue_blocks');

function ea_page_enqueue_styles()
{
    wp_enqueue_style(
        'ea-page-block-style',
        plugins_url('myblock.css', __FILE__),
        array(),
        filemtime(plugin_dir_path(__FILE__) . 'myblock.css')
    );
}

add_action('enqueue_block_assets', 'ea_page_enqueue_styles');

function ea_page_render_block_latest_post_x($attribites)
{
    if (empty($attribites['postIndex'])) {
        return false;
    }
    $index = $attribites['postIndex'];
    $recent_posts = wp_get_recent_posts(array(
        'numberposts' => $index,
        'post_status' => 'publish',
    ));
    if (count($recent_posts) < $index ) {
        return false;
    }

    $post = $recent_posts[$index - 1];
    $post_id = $post['ID'];
    return sprintf(
        '<a class="wp-block-ea-post-preview" href="%1$s">
                    <div class="wp-block-ea-post-preview__fade"></div>
                    <p>%2$s</p>
                    <img alt="Post Preview Image" class="wp-block-ea-post-preview__img" src="%3$s" />
                </a>',
        esc_url(get_permalink($post_id)),
        esc_html(get_the_title($post_id)),
        esc_url(wp_get_attachment_image_src(get_post_thumbnail_id($post_id), 'large')[0])
    );
}

register_block_type('ea-page-blocks/latest-post-x', array(
    'render_callback' => 'ea_page_render_block_latest_post_x',
));
