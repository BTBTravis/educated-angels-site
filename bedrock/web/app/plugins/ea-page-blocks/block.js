var el = wp.element.createElement,
    registerBlockType = wp.blocks.registerBlockType,
    withSelect = wp.data.withSelect;

registerBlockType( 'ea-page-blocks/latest-post-x', {
    title: 'Latest Post X',
    icon: 'format-image',
    category: 'widgets',
    attributes: {
        postIndex: {
            type: 'number',
        },
    },

    edit: function(props) {
        var postIndex = props.attributes.postIndex;
        function updatePost( event ) {
            props.setAttributes( { postIndex: parseInt(event.target.value) } );
        }

        return el(
            'p',
            {},
            'Post Preview with index:',
            el(
                'input',
                { placeholder: 'index of post', value: postIndex, onChange: updatePost },
            ),
        );
    },

    save: function() {
        // Rendering in PHP
        return null;
    },
} );


