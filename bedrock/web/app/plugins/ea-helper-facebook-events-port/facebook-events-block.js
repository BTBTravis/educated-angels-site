var el = wp.element.createElement,
    registerBlockType = wp.blocks.registerBlockType,
    withSelect = wp.data.withSelect;

registerBlockType( 'ea-facebook/events', {
    title: 'Upcoming Facebook Events',
    icon: 'facebook-alt',
    category: 'widgets',

    edit: function() {
        return el(
            'h4',
            {},
            'Upcoming Facebook Events',
        );
    },

    save: function() {
        // Rendering in PHP
        return null;
    },
} );


