<?php
/**
 * Plugin Name: Educated Angels Facebook Events Page Block
 * Plugin URI: https://www.travisshears.com
 * Description: A custom block for rendering facebook events
 * Version: 0.0.1
 * Author: Travis Shears
 * Author URI: https://www.travisshears.com
 **/

function ea_facebook_events_enqueue_scripts()
{
    wp_enqueue_script(
        'ea-facebook-block',
        plugins_url('facebook-events-block.js', __FILE__),
        array('wp-blocks', 'wp-element')
    );
}

add_action('enqueue_block_editor_assets', 'ea_facebook_events_enqueue_scripts');

function ea_facebook_events_enqueue_styles()
{
    wp_enqueue_style(
        'ea-facebook-events-block-style',
        plugins_url('facebook-events-block.css', __FILE__),
        array(),
        filemtime(plugin_dir_path(__FILE__) . 'facebook-events-block.css')
    );
}

add_action('enqueue_block_assets', 'ea_facebook_events_enqueue_styles');

function ea_facebook_events_block_render($attribites)
{
    $handle = curl_init();
    $url = "http://ea-helper/facebook/events";
    // curl -XGET  -H 'accept: application/json' localhost:3002/facebook/events
    curl_setopt_array($handle, [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
    ]);
    $output = curl_exec($handle);
    curl_close($handle);
    if($output === false) {
        return  "<a class='main-link main-link--big' href='https://www.facebook.com/pg/educatedangels/events/?ref=page_internal'>See a list of our events on Facebook</a>";
    }
    $output = json_decode($output, true);
//    echo "<pre>";
//    print_r($output);
//    echo "</pre>";
//    die();
    $templateEvent = function ($evt) {
        $template = '<div class="event-card">
            <p class="event-card__date-month">{{dateMonth}}</p>
            <p class="event-card__date-num">{{dateNum}}</p>
            <p class="event-card__title">{{title}}</p>
            <p class="event-card__sub">{{place}}</p>
            <div class="event-card__foot">
                <p class="event-card__sub">{{city}}, {{state}}</p>
                <a href="{{url}}" class="event-card__link">Learn More</a>
            </div>
        </div>';
        $vals = [
            'dateNum' => $evt['day'],
            'dateMonth' => $evt['month'],
            'title' => $evt['name'],
            'place' => $evt['place']['name'],
            'city' => $evt['place']['location']['city'],
            'state' => $evt['place']['location']['state'],
            'url' => $evt['url'],
        ];
        foreach ($vals as $key => $val) {
            $pattern = '/{{' . $key . '}}/i';
            $template = preg_replace($pattern, $val, $template);
        }
        return $template;
    };

    $html = array_reduce($output, function ($carry, $event) use ($templateEvent) {
        return $carry . $templateEvent($event);
    },'');

    return $html;
}

register_block_type('ea-facebook/events', array(
    'render_callback' => 'ea_facebook_events_block_render',
));
